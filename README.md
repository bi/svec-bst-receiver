# SVEC BST Receiver

This is the gateware for the SVEC BST Receiver, initially designed as a trigger
receiver for the UA9 experiment at CERN. It is based on the following OHWR
hardware components:

  * **SVEC:**    https://ohwr.org/project/svec
  * **TTC_FMC:** https://ohwr.org/project/optical-cdr-fmc

The gateware implements the BST decoder and extracts the following signals on
the front-panel LEMO outputs:

  * **LEMO 1:** bunch clock         (40 MHz, 50% duty cycle)
  * **LEMO 2:** turn clock          (43 kHz, 25 ns pulse)
  * **LEMO 3:** injection pre-pulse (23 us pulse)
  * **LEMO 4:** UA9 trigger         (23 us pulse)

## Hardware configuration

The TTC_FMC should be installed in FMC slot 1 (bottom) on the SVEC. Note that
only V3 of the TTC_FMC is supported.

## Building the gateware

### Required tooling

Building the gateware requires Xilinx ISE 14.7 running on Linux.

[hdlmake] is required in order to create the ISE project.

Generating the MCS file for flashing the PROM requires the *gensdbfs* utility
from the [fpga-config-space] repository. Clone the repository and execute:

```sh
cd fpga-config-space/sdbfs/lib
make
cd ../userspace
make
```

Then copy the *gensdbfs* binary to somewhere in your PATH.

[hdlmake]: https://www.ohwr.org/project/hdl-make
[fpga-config-space]: https://ohwr.org/project/fpga-config-space

### Synthesising the gateware

Generally the following steps are required to be executed from the */hdl/syn/*
directory to synthesise the gateware image:

```sh
./update_sdb.sh # Generate the file ../rtl/sdb_meta_pkg.vhd
hdlmake         # Generate the ISE project
make            # Run ISE to perform the synthesis
```

After ISE has completed, you should end up with the file *svec_top.bit*.

### Loading the gateware

The gateware can be loaded onto a SVEC over VME by running the following
command on the front-end:

```sh
sudo dd if=svec_top.bit of=/dev/svec.0 obs=10M
```

This is assuming the SVEC driver is installed with LUN=0. If not, then adjust
the device number as appropriate.

### Flashing the PROM

To generate the MCS file for flashing the PROM, run the script:

```sh
./build_mcs.sh
```

The resulting *image.mcs* file can be flashed via JTAG to the M25P128 SPI PROM
attached to the XC6SLX9 system FPGA.
