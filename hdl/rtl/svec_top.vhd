--------------------------------------------------------------------------------
-- CERN (BE-BI)
--------------------------------------------------------------------------------
--
-- unit name: svec_top (svec_top.vhd)
--
-- author: Tom Levens (tom.levens@cern.ch)
--
-- date: 18-04-2019
--
-- version: see sdb_meta_pkg.vhd
--
-- description: SVEC top level for the BST Trigger Receiver.
--
-- dependencies:
--
--------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
--------------------------------------------------------------------------------
-- last changes: see git log.
--------------------------------------------------------------------------------
-- TODO: -
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.vme64x_pkg.all;
use work.wishbone_pkg.all;
use work.gencores_pkg.all;
use work.BstDecoderPkg.all;
use work.sdb_meta_pkg.all;

entity svec_top is
  port (
    clk_20m_vcxo_i    : in std_logic;    -- 20MHz VCXO clock
    rst_n_i 		  : in std_logic;

    -- VME interface
    vme_write_n_i     : in    std_logic;
    vme_sysreset_n_i  : in    std_logic;
    --vme_sysclk_i      : in    std_logic;
    vme_retry_oe_o    : out   std_logic;
    vme_retry_n_o     : out   std_logic;
    vme_lword_n_b     : inout std_logic;
    vme_iackout_n_o   : out   std_logic;
    vme_iackin_n_i    : in    std_logic;
    vme_iack_n_i      : in    std_logic;
    vme_gap_i         : in    std_logic;
    vme_dtack_oe_o    : out   std_logic;
    vme_dtack_n_o     : out   std_logic;
    vme_ds_n_i        : in    std_logic_vector(1 downto 0);
    vme_data_oe_n_o   : out   std_logic;
    vme_data_dir_o    : out   std_logic;
    vme_berr_o        : out   std_logic;
    vme_as_n_i        : in    std_logic;
    vme_addr_oe_n_o   : out   std_logic;
    vme_addr_dir_o    : out   std_logic;
    vme_irq_n_o       : out   std_logic_vector(7 downto 1);
    vme_ga_i          : in    std_logic_vector(4 downto 0);
    vme_data_b        : inout std_logic_vector(31 downto 0);
    vme_am_i          : in    std_logic_vector(5 downto 0);
    vme_addr_b        : inout std_logic_vector(31 downto 1);

    -- Carrier 1-wire interface (DS18B20 thermometer + unique ID)
    one_wire_b  : inout std_logic;

    -- Front panel GPIO and LEDs
    led_line_oen_o      : out   std_logic_vector(1 downto 0);
    led_line_o          : out   std_logic_vector(1 downto 0);
    led_column_o        : out   std_logic_vector(3 downto 0);

    gpio_b              : inout std_logic_vector(4 downto 1);
    gpio_term_en_o      : out   std_logic_vector(4 downto 1);
    gpio1_a2b_o         : out   std_logic;
    gpio2_a2b_o         : out   std_logic;
    gpio34_a2b_o        : out   std_logic;

    -- BST FMC
    fmc_pg_c2m_o        : out   std_logic;
    fmc_prsnt_m2c_n_i   : in    std_logic;
    fmc_scl_b           : inout std_logic;
    fmc_sda_b           : inout std_logic;

    fmc_s1_o            : out   std_logic_vector(1 downto 0);
    fmc_s2_o            : out   std_logic_vector(1 downto 0);
    fmc_s3_o            : out   std_logic_vector(1 downto 0);
    fmc_s4_o            : out   std_logic_vector(1 downto 0);
    fmc_s41_sel_o       : out   std_logic;
    fmc_2de_o           : out   std_logic;
    fmc_3de_o           : out   std_logic;
    fmc_div_div4_o      : out   std_logic;
    fmc_div_rst_b_o     : out   std_logic;

    fmc_trig_p_i        : in    std_logic;
    fmc_trig_n_i        : in    std_logic;

    fmc_clk0_m2c_p_i    : in    std_logic;
    fmc_clk0_m2c_n_i    : in    std_logic;
    fmc_cdr_dat_p_i     : in    std_logic;
    fmc_cdr_dat_n_i     : in    std_logic;

    fmc_fpga_scl_b      : inout std_logic;
    fmc_fpga_sda_b      : inout std_logic;

    fmc_user_led_n_o    : out   std_logic;

    fmc_cdr_los_i       : in    std_logic;
    fmc_cdr_lol_i       : in    std_logic
  );
end svec_top;

architecture rtl of svec_top is

  ------------------------------------------------------------------------------
  -- Bit indexes in BST external triggers:
  --   https://wikis.cern.ch/display/BEBI/SPS+BST+Message
  ------------------------------------------------------------------------------

  constant c_BST_PPS_BIT : integer := 4;    -- byte 56, bit 4 (CTG 1 Hz)
  constant c_BST_INJ_BIT : integer := 16;   -- byte 58, bit 0 (inj pre-pulse)
  constant c_BST_UA9_BIT : integer := 21;   -- byte 58, bit 5 (UA9 trigger)

  ------------------------------------------------------------------------------
  -- SDB crossbar constants declaration
  --
  -- WARNING: All address in sdb and crossbar are BYTE addresses!
  ------------------------------------------------------------------------------

  -- Number of master port(s) on the wishbone crossbar
  constant c_NUM_WB_MASTERS   : integer := 1;

  -- Number of slave port(s) on the wishbone crossbar
  constant c_NUM_WB_SLAVES    : integer := 3;

  -- Wishbone master(s)
  constant c_WB_MASTER_VME    : integer := 0;

  -- Wishbone slave(s)
  constant c_WB_SLAVE_I2C     : integer := 0;
  constant c_WB_SLAVE_ONEWIRE : integer := 1;
  constant c_WB_SLAVE_BST     : integer := 2;

  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  constant c_INTERCONNECT_LAYOUT : t_sdb_record_array(5 downto 0) := (
    0 => f_sdb_embed_device(c_xwb_i2c_master_sdb,     x"00001000"),
    1 => f_sdb_embed_device(c_xwb_onewire_master_sdb, x"00001100"),
    2 => f_sdb_embed_device(c_bst_decoder_sdb,        x"00002000"),
    3 => f_sdb_embed_repo_url(c_repo_url_sdb),
    4 => f_sdb_embed_synthesis(c_synthesis_sdb),
    5 => f_sdb_embed_integration(c_integration_sdb)
  );

  ------------------------------------------------------------------------------
  -- Local signals
  ------------------------------------------------------------------------------

  -- System clock
  signal sys_clk_in         : std_logic;
  signal sys_clk_62_5_buf   : std_logic;
  signal sys_clk_62_5       : std_logic;
  signal sys_clk_fb         : std_logic;
  signal sys_clk_pll_locked : std_logic;

  -- Reset
  signal powerup_reset_cnt  : unsigned(7 downto 0) := "00000000";
  signal powerup_rst_n      : std_logic            := '0';
  signal sys_rst_n          : std_logic;
  signal sys_rst            : std_logic;

  -- VME interface
  signal vme_i              : t_vme64x_in;
  signal vme_o              : t_vme64x_out;

  -- Wishbone buses
  signal cnx_master_out     : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in      : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_slave_out      : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in       : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- Carrier 1-wire
  signal one_wire_en        : std_logic_vector(0 downto 0);
  signal one_wire_i         : std_logic_vector(0 downto 0);

  -- FMC I2C
  signal fmc_scl_in         : std_logic_vector(1 downto 0);
  signal fmc_scl_out        : std_logic_vector(1 downto 0);
  signal fmc_scl_oe_n       : std_logic_vector(1 downto 0);
  signal fmc_sda_in         : std_logic_vector(1 downto 0);
  signal fmc_sda_out        : std_logic_vector(1 downto 0);
  signal fmc_sda_oe_n       : std_logic_vector(1 downto 0);

  -- BST
  signal cdr_clk            : std_logic;
  signal cdr_dat            : std_logic;

  signal bst_clk            : std_logic;
  signal bst_rst            : std_logic;

  signal bst_tc_flag        : std_logic;
  signal bst_bc_flag        : std_logic;
  signal bst_tc_flag_dly    : std_logic;
  signal bst_bc_flag_dly    : std_logic;
  signal bst_tc             : std_logic;
  signal bst_bc             : std_logic;

  signal bst_addr           : std_logic_vector(7 downto 0);
  signal bst_data           : std_logic_vector(7 downto 0);
  signal bst_str            : std_logic;
  signal bst_err            : std_logic;

  signal bst_ext_trig       : std_logic_vector(23 downto 0);

  signal ext_trig           : std_logic;

  -- LEDs
  constant c_LED_OFF        : std_logic_vector(1 downto 0) := "00";
  constant c_LED_RED        : std_logic_vector(1 downto 0) := "10";
  constant c_LED_GRN        : std_logic_vector(1 downto 0) := "01";
  constant c_LED_ORN        : std_logic_vector(1 downto 0) := "11";

  signal led1               : std_logic_vector(1 downto 0);
  signal led2               : std_logic_vector(1 downto 0);
  signal led3               : std_logic_vector(1 downto 0);
  signal led4               : std_logic_vector(1 downto 0);
  signal led5               : std_logic_vector(1 downto 0);
  signal led6               : std_logic_vector(1 downto 0);
  signal led7               : std_logic_vector(1 downto 0);
  signal led8               : std_logic_vector(1 downto 0);
  signal led_fmc            : std_logic;

  signal leds_x0            : std_logic_vector(16 downto 0);
  signal leds_x1            : std_logic_vector(16 downto 0);
  signal leds               : std_logic_vector(16 downto 0);

begin

  ------------------------------------------------------------------------------
  -- Clocks distribution from 20MHz TCXO
  --  62.500 MHz system clock
  ------------------------------------------------------------------------------

  cmp_sys_clk_buf : IBUFG
    port map (
      I => clk_20m_vcxo_i,
      O => sys_clk_in);

  cmp_sys_clk_pll : PLL_BASE
    generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLK_FEEDBACK       => "CLKFBOUT",
      COMPENSATION       => "INTERNAL",
      DIVCLK_DIVIDE      => 1,
      CLKFBOUT_MULT      => 50,
      CLKFBOUT_PHASE     => 0.000,
      CLKOUT0_DIVIDE     => 8,
      CLKOUT0_PHASE      => 0.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKOUT1_DIVIDE     => 16,
      CLKOUT1_PHASE      => 0.000,
      CLKOUT1_DUTY_CYCLE => 0.500,
      CLKOUT2_DIVIDE     => 3,
      CLKOUT2_PHASE      => 0.000,
      CLKOUT2_DUTY_CYCLE => 0.500,
      CLKIN_PERIOD       => 50.0,
      REF_JITTER         => 0.016)
    port map (
      CLKFBOUT => sys_clk_fb,
      CLKOUT0  => open,
      CLKOUT1  => sys_clk_62_5_buf,
      CLKOUT2  => open,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,
      LOCKED   => sys_clk_pll_locked,
      RST      => '0',
      CLKFBIN  => sys_clk_fb,
      CLKIN    => sys_clk_in);

  cmp_clk_62_5_buf : BUFG
    port map (
      O => sys_clk_62_5,
      I => sys_clk_62_5_buf);

  ------------------------------------------------------------------------------
  -- System reset
  ------------------------------------------------------------------------------

  p_powerup_reset : process(sys_clk_62_5)
  begin
    if rising_edge(sys_clk_62_5) then
      if (vme_sysreset_n_i = '0' or rst_n_i = '0') then
        powerup_rst_n <= '0';
      elsif sys_clk_pll_locked = '1' then
        if (powerup_reset_cnt = "11111111") then
          powerup_rst_n <= '1';
        else
          powerup_rst_n     <= '0';
          powerup_reset_cnt <= powerup_reset_cnt + 1;
        end if;
      else
        powerup_rst_n     <= '0';
        powerup_reset_cnt <= "00000000";
      end if;
    end if;
  end process;

  sys_rst   <= not powerup_rst_n;
  sys_rst_n <= powerup_rst_n;

  ------------------------------------------------------------------------------
  -- VME interface
  ------------------------------------------------------------------------------

  cmp_vme_core : xvme64x_core
    generic map (
      g_CLOCK_PERIOD    => 16,  -- ns
      g_DECODE_AM       => true,
      g_ENABLE_CR_CSR   => true,
      g_USER_CSR_EXT    => false,
      g_WB_GRANULARITY  => BYTE,
      g_MANUFACTURER_ID => c_CERN_ID,
      g_BOARD_ID        => c_SVEC_ID,
      g_REVISION_ID     => c_SVEC_REVISION_ID,
      g_PROGRAM_ID      => c_SVEC_PROGRAM_ID)
    port map (
      clk_i             => sys_clk_62_5,
      rst_n_i           => sys_rst_n,
      rst_n_o           => open,
      vme_i             => vme_i,
      vme_o             => vme_o,
      wb_i              => cnx_master_in(c_WB_MASTER_VME),
      wb_o              => cnx_master_out(c_WB_MASTER_VME),
      int_i             => '0',
      irq_ack_o         => open);

  vme_i.as_n      <= vme_as_n_i;
  vme_i.rst_n     <= vme_sysreset_n_i;
  vme_i.write_n   <= vme_write_n_i;
  vme_i.am        <= vme_am_i;
  vme_i.ds_n      <= vme_ds_n_i;
  vme_i.ga        <= vme_gap_i & vme_ga_i;
  vme_i.lword_n   <= vme_lword_n_b;
  vme_i.data      <= vme_data_b;
  vme_i.addr      <= vme_addr_b;
  vme_i.iack_n    <= vme_iack_n_i;
  vme_i.iackin_n  <= vme_iackin_n_i;

  vme_retry_oe_o  <= vme_o.retry_oe;
  vme_retry_n_o   <= vme_o.retry_n;
  vme_iackout_n_o <= vme_o.iackout_n;
  vme_dtack_oe_o  <= vme_o.dtack_oe;
  vme_dtack_n_o   <= vme_o.dtack_n;
  vme_data_oe_n_o <= vme_o.data_oe_n;
  vme_data_dir_o  <= vme_o.data_dir;
  vme_berr_o      <= not vme_o.berr_n;
  vme_addr_oe_n_o <= vme_o.addr_oe_n;
  vme_addr_dir_o  <= vme_o.addr_dir;
  vme_irq_n_o     <= not vme_o.irq_n;

  vme_data_b      <= vme_o.data    when vme_o.data_dir = '1' else (others => 'Z');
  vme_addr_b      <= vme_o.addr    when vme_o.addr_dir = '1' else (others => 'Z');
  vme_lword_n_b   <= vme_o.lword_n when vme_o.addr_dir = '1' else 'Z';

  ------------------------------------------------------------------------------
  -- Wishbone crossbar
  ------------------------------------------------------------------------------

  cmp_sdb_crossbar : xwb_sdb_crossbar
    generic map (
      g_num_masters => c_NUM_WB_MASTERS,
      g_num_slaves  => c_NUM_WB_SLAVES,
      g_registered  => true,
      g_wraparound  => true,
      g_layout      => c_INTERCONNECT_LAYOUT,
      g_sdb_addr    => c_SDB_ADDRESS)
    port map (
      clk_sys_i => sys_clk_62_5,
      rst_n_i   => sys_rst_n,
      slave_i   => cnx_master_out,
      slave_o   => cnx_master_in,
      master_i  => cnx_slave_out,
      master_o  => cnx_slave_in);

  ------------------------------------------------------------------------------
  -- Carrier 1-wire master
  --    DS18B20 (thermometer + unique ID)
  ------------------------------------------------------------------------------

  cmp_carrier_onewire : xwb_onewire_master
    generic map (
      g_interface_mode      => CLASSIC,
      g_address_granularity => BYTE,
      g_num_ports           => 1,
      g_ow_btp_normal       => "5.0",
      g_ow_btp_overdrive    => "1.0")
    port map (
      clk_sys_i   => sys_clk_62_5,
      rst_n_i     => sys_rst_n,
      slave_i     => cnx_slave_in(c_WB_SLAVE_ONEWIRE),
      slave_o     => cnx_slave_out(c_WB_SLAVE_ONEWIRE),
      desc_o      => open,
      owr_pwren_o => open,
      owr_en_o    => one_wire_en,
      owr_i       => one_wire_i);

  one_wire_b    <= '0' when one_wire_en(0) = '1' else 'Z';
  one_wire_i(0) <= one_wire_b;

  ------------------------------------------------------------------------------
  -- I2C master
  --    FMC EEPROM I2C bus
  --    FMC CDR I2C bus
  ------------------------------------------------------------------------------

  cmp_fmc_i2c_master : xwb_i2c_master
    generic map (
      g_interface_mode      => CLASSIC,
      g_address_granularity => BYTE,
      g_num_interfaces      => 2)
    port map (
      clk_sys_i     => sys_clk_62_5,
      rst_n_i       => sys_rst_n,

      slave_i       => cnx_slave_in(c_WB_SLAVE_I2C),
      slave_o       => cnx_slave_out(c_WB_SLAVE_I2C),
      desc_o        => open,

      scl_pad_i     => fmc_scl_in,
      scl_pad_o     => fmc_scl_out,
      scl_padoen_o  => fmc_scl_oe_n,
      sda_pad_i     => fmc_sda_in,
      sda_pad_o     => fmc_sda_out,
      sda_padoen_o  => fmc_sda_oe_n);

  -- Tri-state buffers for SDA and SCL
  fmc_scl_b      <= fmc_scl_out(0) when fmc_scl_oe_n(0) = '0' else 'Z';
  fmc_fpga_scl_b <= fmc_scl_out(1) when fmc_scl_oe_n(1) = '0' else 'Z';
  fmc_scl_in     <= fmc_fpga_scl_b & fmc_scl_b;

  fmc_sda_b      <= fmc_sda_out(0) when fmc_sda_oe_n(0) = '0' else 'Z';
  fmc_fpga_sda_b <= fmc_sda_out(1) when fmc_sda_oe_n(1) = '0' else 'Z';
  fmc_sda_in     <= fmc_fpga_sda_b & fmc_sda_b;

  ------------------------------------------------------------------------------
  -- BST decoder
  ------------------------------------------------------------------------------

  -- Differential input buffers
  cmp_cdr_clk_buf : IBUFGDS
    generic map (
      DIFF_TERM     => true,
      IBUF_LOW_PWR  => false,
      IOSTANDARD    => "DEFAULT")
    port map (
      O   => cdr_clk,
      I   => fmc_clk0_m2c_p_i,
      IB  => fmc_clk0_m2c_n_i);

  cmp_cdr_dat_buf : IBUFDS
    generic map (
      DIFF_TERM     => true,
      IBUF_LOW_PWR  => false,
      IOSTANDARD    => "DEFAULT")
    port map (
      O   => cdr_dat,
      I   => fmc_cdr_dat_p_i,
      IB  => fmc_cdr_dat_n_i);

  cmp_ext_trig_buf : IBUFDS
    generic map (
      DIFF_TERM     => true,
      IBUF_LOW_PWR  => false,
      IOSTANDARD    => "DEFAULT")
    port map (
      O   => ext_trig,
      I   => fmc_trig_p_i,
      IB  => fmc_trig_n_i);

  -- Configure FMC
  fmc_s1_o        <= "11";  -- CDR_CLK => PCB TP (R5)
  fmc_s2_o        <= "11";  -- CDR_CLK => CLK1_M2C
  fmc_s3_o        <= "11";  -- CDR_CLK => CLK2_BIDIR
  fmc_s4_o        <= "11";  -- CDR_CLK => CLK0_M2C
  fmc_s41_sel_o   <= '0';
  fmc_2de_o       <= '0';
  fmc_3de_o       <= '0';
  fmc_div_div4_o  <= '0';
  fmc_div_rst_b_o <= '0';
  fmc_pg_c2m_o    <= '1';

  -- BST decoder core
  cmp_bst_decoder : BstDecoder
    port map (
      WbClk_ik            => sys_clk_62_5,
      WbRst_ir            => sys_rst,
      WbCyc_i             => cnx_slave_in(c_WB_SLAVE_BST).cyc,
      WbStb_i             => cnx_slave_in(c_WB_SLAVE_BST).stb,
      WbWe_i              => cnx_slave_in(c_WB_SLAVE_BST).we,
      WbAdr_ib9           => cnx_slave_in(c_WB_SLAVE_BST).adr(10 downto 2),
      WbDat_ib32          => cnx_slave_in(c_WB_SLAVE_BST).dat,
      WbDat_ob32          => cnx_slave_out(c_WB_SLAVE_BST).dat,
      WbAck_o             => cnx_slave_out(c_WB_SLAVE_BST).ack,

      CdrLos_i            => fmc_cdr_los_i,
      CdrLol_i            => fmc_cdr_lol_i,
      SfpPresent_i        => '1',
      SfpTxFault_i        => '0',
      SfpLos_i            => '0',
      SfpTxDisable_i      => '0',
      SfpRateSelect_i     => '0',

      Reset_iar           => sys_rst,

      CdrClk_ik           => cdr_clk,
      CdrDat_i            => cdr_dat,

      BstClk_ok           => bst_clk,
      BstRst_or           => bst_rst,
      BunchClkFlag_o      => bst_bc_flag,
      TurnClkFlag_o       => bst_tc_flag,
      BunchClkFlagDly_o   => bst_bc_flag_dly,
      TurnClkFlagDly_o    => bst_tc_flag_dly,

      BstByteAddress_ob8  => bst_addr,
      BstByteData_ob8     => bst_data,
      BstByteStrobe_o     => bst_str,
      BstByteError_o      => bst_err);

  cnx_slave_out(c_WB_SLAVE_BST).err   <= '0';
  cnx_slave_out(c_WB_SLAVE_BST).rty   <= '0';
  cnx_slave_out(c_WB_SLAVE_BST).stall <= '0';

  --               _   _   _   _   _   _   _   _  / /  _   _   _   _   _
  -- bst_clk     _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_\ \_/ \_/ \_/ \_/ \_/ \_
  --               ___             ___            / /      ___
  -- bst_bc_flag _/   \___________/   \___________\ \_____/   \___________
  --               ___                            / /      ___
  -- bst_tc_flag _/   \___________________________\ \_____/   \___________
  --                   _______         _______    / /          _______
  -- bst_bc      _____/       \_______/       \___\ \_________/       \___
  --                   _______________            / /          ___________
  -- bst_tc      _____/               \___________\ \_________/
  --                                              / /
  cmp_bst_clk_conv : BstClkConv
    port map (
      BstClk_ik         => bst_clk,
      BstRst_ir         => bst_rst,
      BunchClkFlag_i    => bst_bc_flag_dly,
      TurnClkFlag_i     => bst_tc_flag_dly,
      BunchClk_o        => bst_bc,
      TurnClk_o         => bst_tc);

  -- Decode BST external triggers
  cmp_bst_ext_trig_reg : BstRegister
    generic map (
      g_NbBytes           => 3)
    port map (
      BstClk_ik           => bst_clk,
      BstRst_ir           => bst_rst,
      TurnClkFlag_i       => bst_tc_flag,
      TurnClkFlagDly_i    => bst_tc_flag_dly,
      BstByteAddress_ib8  => bst_addr,
      BstByteData_ib8     => bst_data,
      BstByteStrobe_i     => bst_str,
      BstByteError_i      => bst_err,
      RegisterAddr_ib8    => x"38", -- 56
      RegisterData_ob     => bst_ext_trig);

  ------------------------------------------------------------------------------
  -- Front panel GPIO
  ------------------------------------------------------------------------------

  gpio_b <= (1 => bst_bc,
             2 => bst_tc,
             3 => bst_ext_trig(c_BST_INJ_BIT),
             4 => bst_ext_trig(c_BST_UA9_BIT));

  gpio_term_en_o <= "0000";
  gpio1_a2b_o    <= '1'; -- Output
  gpio2_a2b_o    <= '1'; -- Output
  gpio34_a2b_o   <= '1'; -- Output

  ------------------------------------------------------------------------------
  -- LEDs
  ------------------------------------------------------------------------------

  -- LED encoding
  led1 <= c_LED_RED when fmc_cdr_los_i = '1' else c_LED_GRN;
  led2 <= c_LED_RED when fmc_cdr_lol_i = '1' else c_LED_GRN;
  led3 <= c_LED_OFF;
  led4 <= c_LED_GRN when bst_ext_trig(c_BST_PPS_BIT) = '1' else c_LED_OFF;
  led5 <= c_LED_OFF;
  led6 <= c_LED_OFF;
  led7 <= c_LED_GRN when bst_ext_trig(c_BST_INJ_BIT) = '1' else c_LED_OFF;
  led8 <= c_LED_GRN when bst_ext_trig(c_BST_UA9_BIT) = '1' else c_LED_OFF;

  led_fmc <= bst_ext_trig(c_BST_PPS_BIT);

  -- Synchronise LEDs to system clock
  p_leds_sync : process(sys_clk_62_5)
  begin
    if rising_edge(sys_clk_62_5) then
      if (sys_rst = '1') then
        leds_x0 <= '0' & x"0000";
        leds_x1 <= '0' & x"0000";
      else
        leds_x0 <= led_fmc &
                   led5 & led6 & led7 & led8 &
                   led1 & led2 & led3 & led4;
        leds_x1 <= leds_x0;
      end if;
    end if;
  end process;

  -- Extend LED pulses to 100ms
  g_leds_extend: for i in 0 to 16 generate
    cmp_leds_extend : gc_extend_pulse
      generic map (
        g_width     => 6250000)
      port map (
        clk_i       => sys_clk_62_5,
        rst_n_i     => sys_rst_n,
        pulse_i     => leds_x1(i),
        extended_o  => leds(i));
  end generate g_leds_extend;

  -- LED controller
  cmp_led_controller : gc_bicolor_led_ctrl
    generic map (
      g_nb_column     => 4,
      g_nb_line       => 2,
      g_clk_freq      => 62500000,  -- in Hz
      g_refresh_rate  => 250)       -- in Hz
    port map (
      rst_n_i         => sys_rst_n,
      clk_i           => sys_clk_62_5,
      led_intensity_i => "1100100", -- in %
      led_state_i     => leds(15 downto 0),
      column_o        => led_column_o,
      line_o          => led_line_o,
      line_oen_o      => led_line_oen_o);

  -- FMC user LED
  fmc_user_led_n_o <= not leds(16);

end rtl;
