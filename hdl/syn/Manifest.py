target = "xilinx"
action = "synthesis"

syn_device  = "xc6slx150t"
syn_grade   = "-3"
syn_package = "fgg900"
syn_top     = "svec_top"
syn_project = "svec_top.xise"
syn_tool    = "ise"

files = [
    "../svec_top.ucf"
]

syn_pre_synthesize_cmd = "./update_sdb.sh"
syn_post_bitstream_cmd = "./build_mcs.sh"

modules = {
    "local" : [
        "../rtl",
        "../ip_cores/general-cores",
        "../ip_cores/vme64x-core",
        "../ip_cores/BST_FPGA"
    ]
}

fetchto = "../ip_cores"

