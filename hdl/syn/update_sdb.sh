#!/bin/bash

SYN_MODULE=svec_top
SYN_TOOL_NAME=ISE
SYN_TOOL_VERSION=14.7

NAME=svec-bst-receiver
VENDOR_ID=CE42
VERSION=1.0

SDB=../rtl/sdb_meta_pkg.vhd

if [ -z "$SDB" ]; then
    echo "usage: $0 filename" >&2
    exit 1
fi

DEVICE_ID=$( echo $NAME | md5sum | cut -c1-8 )
REPO_URL=$( git remote -v | awk '$1 == "origin" && $3 == "(fetch)" { print $2 }' )
COMMIT_ID=$( git log -1 --format="%H" | cut -c1-32 )

function enc_str {
    printf "%-${2}s" $( echo "$1" | cut -c1-$2 )
}

function enc_bcd {
    printf "%0${2}X" 0x$( echo "$1" | cut -c1-$2 )
}

function enc_ver {
    printf '%04X%04X' 0x$( echo $1 | cut -d. -f1 ) 0x$( echo $1 | cut -d. -f2 )
}

HDL_REPO_URL=$( enc_str "$REPO_URL" 63 )

HDL_SYN_COMMIT_ID=$( enc_str "$COMMIT_ID" 32 )
HDL_SYN_MODULE_NAME=$( enc_str "$SYN_MODULE" 16 )
HDL_SYN_TOOL_NAME=$( enc_str "$SYN_TOOL_NAME" 8 )
HDL_SYN_TOOL_VERSION=$( enc_bcd $( echo $SYN_TOOL_VERSION | sed 's/\.//g' ) 8 )
HDL_SYN_USERNAME=$( enc_str "$( logname )" 15 )
HDL_SYN_DATE=$( enc_bcd $( date +%Y%m%d ) 8 )

HDL_INTEGR_VENDOR_ID=$( enc_bcd $VENDOR_ID 16 )
HDL_INTEGR_DEVICE_ID=$( enc_bcd $DEVICE_ID 8 )
HDL_INTEGR_VERSION=$( enc_ver $VERSION )
HDL_INTEGR_NAME=$( enc_str "$NAME" 19 )
HDL_INTEGR_DATE=$( enc_bcd $( date +%Y%m%d ) 8 )

cat << EOF > $SDB
--------------------------------------------------------------------------------
-- SDB meta-information records
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.wishbone_pkg.all;

package sdb_meta_pkg is

  -- Top module repository url
  constant c_repo_url_sdb : t_sdb_repo_url := (
    -- Repo URL (string, 63 char)
    repo_url => "$HDL_REPO_URL");

  -- Synthesis informations
  constant c_synthesis_sdb : t_sdb_synthesis := (
    -- Top module name (string, 16 char)
    syn_module_name  => "$HDL_SYN_MODULE_NAME",
    -- Commit ID (hex string, 128-bit = 32 char)
    -- git log -1 --format="%H" | cut -c1-32
    syn_commit_id    => "$HDL_SYN_COMMIT_ID",
    -- Synthesis tool name (string, 8 char)
    syn_tool_name    => "$HDL_SYN_TOOL_NAME",
    -- Synthesis tool version (bcd encoded, 32-bit)
    syn_tool_version => x"$HDL_SYN_TOOL_VERSION",
    -- Synthesis date (bcd encoded, 32-bit)
    syn_date         => x"$HDL_SYN_DATE",
    -- Synthesised by (string, 15 char)
    syn_username     => "$HDL_SYN_USERNAME");

  -- Integration record
  constant c_integration_sdb : t_sdb_integration := (
    product     => (
      vendor_id => x"$HDL_INTEGR_VENDOR_ID",
      device_id => x"$HDL_INTEGR_DEVICE_ID",              -- echo "$NAME" | md5sum | cut -c1-8
      version   => x"$HDL_INTEGR_VERSION",              -- bcd encoded, [31:16] = major, [15:0] = minor
      date      => x"$HDL_INTEGR_DATE",              -- yyyymmdd
      name      => "$HDL_INTEGR_NAME"));  -- string, 19 char

end sdb_meta_pkg;

package body sdb_meta_pkg is
end sdb_meta_pkg;
EOF
